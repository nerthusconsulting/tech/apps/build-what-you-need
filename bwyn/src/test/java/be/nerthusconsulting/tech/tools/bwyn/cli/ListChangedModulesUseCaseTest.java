package be.nerthusconsulting.tech.tools.bwyn.cli;

import be.nerthusconsulting.tech.tools.bwyn.TestInDirectoryExtension;
import org.assertj.core.api.Assertions;
import org.eclipse.jgit.revwalk.RevCommit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.io.ByteArrayOutputStream;
import java.nio.file.Path;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class ListChangedModulesUseCaseTest {
    @RegisterExtension
    TestInDirectoryExtension testInDirectoryExtension = new TestInDirectoryExtension();

    Path gitProjectDirectory;

    @BeforeEach
    void beforeEach() {
        this.gitProjectDirectory = testInDirectoryExtension.directory();
    }
    
    @Test
    void shouldListProjectB_whenRunBetweenHeadAndFirstCommit() throws Exception {
        ByteArrayOutputStream redirectedSystemOut = BWYNTestUtil.redirectSystemOut();

        List<RevCommit> commits = BWYNTestUtil.setUpGitRepository(gitProjectDirectory);
        RevCommit firstCommit = commits.get(0);

        BWYN.main(firstCommit.getId().getName());

        Assertions.assertThat(BWYNTestUtil.firstLine(redirectedSystemOut))
                  .isEqualTo("Les modules suivants ont été modifiés entre les références " +
                             firstCommit.getId().getName() +
                             " et HEAD : " +
                             BWYNTestUtil.PROJECT_A_NAME +
                             "," +
                             BWYNTestUtil.PROJECT_B_NAME);
    }

    @Test
    void shouldListProjectB_whenRunBetweenSecondAndFirstCommit() throws Exception {
        ByteArrayOutputStream redirectedSystemOut = BWYNTestUtil.redirectSystemOut();

        List<RevCommit> commits = BWYNTestUtil.setUpGitRepository(gitProjectDirectory);
        RevCommit firstCommit = commits.get(0);
        RevCommit secondCommit = commits.get(1);

        BWYN.main(firstCommit.getId().getName(),
                  secondCommit.getId().getName());

        Assertions.assertThat(BWYNTestUtil.firstLine(redirectedSystemOut))
                  .isEqualTo("Le module suivant a été modifié entre les références " +
                             firstCommit.getId().getName() +
                             " et " +
                             secondCommit.getId().getName() +
                             " : " +
                             BWYNTestUtil.PROJECT_B_NAME);
    }

    @Test
    void shouldListProjectAAndB_whenRunBetweenFourthAndFirstCommit() throws Exception {
        ByteArrayOutputStream redirectedSystemOut = BWYNTestUtil.redirectSystemOut();

        List<RevCommit> commits = BWYNTestUtil.setUpGitRepository(gitProjectDirectory);
        RevCommit firstCommit = commits.get(0);
        RevCommit fourthCommit = commits.get(4);

        BWYN.main(firstCommit.getId().getName(),
                  fourthCommit.getId().getName());

        Assertions.assertThat(BWYNTestUtil.firstLine(redirectedSystemOut))
                  .isEqualTo("Les modules suivants ont été modifiés entre les références " +
                             firstCommit.getId().getName() +
                             " et " +
                             fourthCommit.getId().getName() +
                             " : " +
                             BWYNTestUtil.PROJECT_A_NAME +
                             "," +
                             BWYNTestUtil.PROJECT_B_NAME);
    }

    @Test
    void shouldWarnUserAboutNoModulesHavingBeenChanged_whenRunBetweenThirdAndSecondCommit() throws Exception {
        ByteArrayOutputStream redirectedSystemOut = BWYNTestUtil.redirectSystemOut();

        List<RevCommit> commits = BWYNTestUtil.setUpGitRepository(gitProjectDirectory);
        RevCommit secondCommit = commits.get(1);
        RevCommit thirdCommit = commits.get(2);

        BWYN.main(secondCommit.getId().getName(),
                  thirdCommit.getId().getName());

        Assertions.assertThat(BWYNTestUtil.firstLine(redirectedSystemOut))
                  .isEqualTo("Aucun module n'a été modifié entre les références " +
                             secondCommit.getId().getName() +
                             " et " +
                             thirdCommit.getId().getName());
    }

    @Test
    void shouldWarnUserAboutNonExistingReference_whenRunWithFirstArgMatchingNoReference() throws Exception {
        ByteArrayOutputStream redirectedSystemOut = BWYNTestUtil.redirectSystemOut();

        BWYNTestUtil.setUpGitRepository(gitProjectDirectory);

        BWYN.main("nonExistingReference");

        Assertions.assertThat(BWYNTestUtil.firstLine(redirectedSystemOut))
                  .isEqualTo(
                          "La référence nonExistingReference n'existe pas. Veuillez vérifier que vous ne vous êtes pas trompé");
    }

    @Test
    void shouldWarnUserAboutNonExistingReference_whenRunWithSecondArgMatchingNoReference() throws Exception {
        ByteArrayOutputStream redirectedSystemOut = BWYNTestUtil.redirectSystemOut();

        List<RevCommit> commits = BWYNTestUtil.setUpGitRepository(gitProjectDirectory);
        RevCommit firstCommit = commits.get(0);

        BWYN.main(firstCommit.getId().getName(), "nonExistingReference");

        Assertions.assertThat(BWYNTestUtil.firstLine(redirectedSystemOut))
                  .isEqualTo(
                          "La référence nonExistingReference n'existe pas. Veuillez vérifier que vous ne vous êtes pas trompé");
    }

    @Test
    void shouldWarnUserAboutNonExistingReference_whenRunWithBothArgsMatchingNoReference() throws Exception {
        ByteArrayOutputStream redirectedSystemOut = BWYNTestUtil.redirectSystemOut();

        BWYNTestUtil.setUpGitRepository(gitProjectDirectory);

        BWYN.main("firstNonExistingReference", "secondNonExistingReference");

        Assertions.assertThat(BWYNTestUtil.firstLine(redirectedSystemOut))
                  .isEqualTo(
                          "Les références firstNonExistingReference et secondNonExistingReference n'existent pas. Veuillez vérifier que vous ne vous êtes pas trompé");
    }

    @Test
    void shouldWarnUserAboutGivingTooManyReferences_whenRunWithMoreThanTwoArguments() throws Exception {
        ByteArrayOutputStream redirectSystemOut = BWYNTestUtil.redirectSystemOut();

        List<RevCommit> commits = BWYNTestUtil.setUpGitRepository(gitProjectDirectory);
        RevCommit firstCommit = commits.get(0);
        RevCommit secondCommit = commits.get(1);

        BWYN.main(firstCommit.getId().getName(),
                  secondCommit.getId().getName(),
                  "thirdReference");

        Assertions.assertThat(BWYNTestUtil.firstLine(redirectSystemOut))
                  .isEqualTo("BWYN accepte entre une et deux références, 3 ont été fournies");
    }

    @Test
    void shouldWarnUserAboutGivingNoReference_whenRunWithNoArguments() throws Exception {
        ByteArrayOutputStream redirectSystemOut = BWYNTestUtil.redirectSystemOut();

        BWYNTestUtil.setUpGitRepository(gitProjectDirectory);

        BWYN.main();

        Assertions.assertThat(BWYNTestUtil.firstLine(redirectSystemOut))
                  .isEqualTo("BWYN accepte entre une et deux références, aucune n'a été fournie");
    }
}
