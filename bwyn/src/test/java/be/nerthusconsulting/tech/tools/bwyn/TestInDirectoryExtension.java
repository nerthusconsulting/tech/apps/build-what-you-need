package be.nerthusconsulting.tech.tools.bwyn;

import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.nio.file.Files;
import java.nio.file.Path;

public class TestInDirectoryExtension implements BeforeEachCallback, AfterEachCallback {
    private static final String WORKING_DIRECTORY_PROPERTY = "user.dir";

    private Path directory;
    private Path initialDirectory;

    @Override
    public void beforeEach(ExtensionContext context) throws Exception {
        this.directory = Files.createTempDirectory("");
        this.initialDirectory = currentDirectory();
        System.setProperty(WORKING_DIRECTORY_PROPERTY, directory.toString());
    }

    private static Path currentDirectory() {
        return Path.of(".").toAbsolutePath();
    }

    @Override
    public void afterEach(ExtensionContext context) {
        System.setProperty(WORKING_DIRECTORY_PROPERTY, initialDirectory.toString());
        this.initialDirectory = null;
    }

    public Path directory() {
        return directory;
    }
}
