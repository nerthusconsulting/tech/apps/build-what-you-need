package be.nerthusconsulting.tech.tools.bwyn;

@FunctionalInterface
public interface ThrowingRunnable {
    void run() throws Exception;
}
