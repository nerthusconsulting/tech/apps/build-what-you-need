package be.nerthusconsulting.tech.tools.bwyn.cli;

import be.nerthusconsulting.tech.tools.bwyn.TestInDirectoryExtension;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.revwalk.RevCommit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class BWYNTestUtil {
    public static final String PROJECT_A_NAME = "project-a";
    public static final String PROJECT_B_NAME = "project-b";
    private static final String INITIAL_POMS_DIRECTORY = "be/nerthusconsulting/tech/tools/bwyn/cli/";

    public static ByteArrayOutputStream redirectSystemOut() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));
        return outputStream;
    }

    public static List<RevCommit> setUpGitRepository(Path gitProjectDirectory) throws GitAPIException, IOException {
        Git git = initRepository(gitProjectDirectory);
        return List.of(createAndCommitProjectA(git, gitProjectDirectory),
                       createAndCommitProjectB(git, gitProjectDirectory),
                       addFileAndCommit(git, PROJECT_B_NAME, "README.md", gitProjectDirectory),
                       addSourceFileAndCommit(git, PROJECT_B_NAME, "Example.java", gitProjectDirectory),
                       addSourceFileAndCommit(git, PROJECT_A_NAME, "Test.java", gitProjectDirectory));
    }

    private static Git initRepository(Path gitProjectDirectory) throws GitAPIException {
        return Git.init().setDirectory(gitProjectDirectory.toFile()).call();
    }

    private static RevCommit createAndCommitProjectA(Git git, Path gitProjectDirectory) throws IOException, GitAPIException {
        createProjectA(gitProjectDirectory);
        return commitProject(git, PROJECT_A_NAME, "Project A");
    }

    private static void createProjectA(Path gitProjectDirectory) throws IOException {
        createParentPom(gitProjectDirectory);
        createProject(PROJECT_A_NAME, gitProjectDirectory);
    }

    private static void createParentPom(Path gitProjectDirectory) throws IOException {
        copyPom("parent-pom-only-project-a.xml", gitProjectDirectory);
    }

    private static void copyPom(String fileName, Path targetDirectory) throws IOException {
        try (InputStream source = BWYNTestUtil.class.getClassLoader().getResourceAsStream(INITIAL_POMS_DIRECTORY + fileName)) {
            assertThat(source).isNotNull();
            Path target = targetDirectory.resolve("pom.xml");
            Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
        }
    }

    private static void createProject(String projectName, Path gitProjectDirectory) throws IOException {
        Files.createDirectory(projectDirectory(projectName, gitProjectDirectory));
        copyPom(projectPomFileName(projectName), projectDirectory(projectName, gitProjectDirectory));
    }

    private static String projectPomFileName(String projectName) {
        return "pom-" + projectName + ".xml";
    }

    private static Path projectDirectory(String projectDirectoryName, Path gitProjectDirectory) {
        return gitProjectDirectory.resolve(projectDirectoryName);
    }

    private static void createFile(String projectDirectoryName, String fileName, Path gitProjectDirectory) throws IOException {
        Files.createFile(projectDirectory(projectDirectoryName, gitProjectDirectory).resolve(fileName));
    }

    private static RevCommit commitProject(Git git, String projectDirectoryName, String message) throws GitAPIException {
        git.add()
           .addFilepattern(projectDirectoryName)
           .call();
        return git.commit()
                  .setCommitter("francois.dupire", "francois.dupire@nerthusconsulting.be")
                  .setMessage(message)
                  .call();
    }

    private static RevCommit createAndCommitProjectB(Git git, Path gitProjectDirectory) throws IOException, GitAPIException {
        createProjectB(gitProjectDirectory);
        return commitProject(git, PROJECT_B_NAME, "Project B");
    }

    private static void createProjectB(Path gitProjectDirectory) throws IOException {
        updateParentPom(gitProjectDirectory);
        createProject(PROJECT_B_NAME, gitProjectDirectory);
    }

    private static void updateParentPom(Path gitProjectDirectory) throws IOException {
        copyPom("parent-pom-both-projects.xml", gitProjectDirectory);
    }

    private static RevCommit addFileAndCommit(Git git,
                                              String projectDirectoryName,
                                              String fileName,
                                              Path gitProjectDirectory) throws IOException,
                                                                               GitAPIException {
        createFile(projectDirectoryName, fileName, gitProjectDirectory);
        return commitProject(git, projectDirectoryName, "Yet another file");
    }

    private static RevCommit addSourceFileAndCommit(Git git,
                                                    String projectDirectoryName,
                                                    String fileName,
                                                    Path gitProjectDirectory) throws IOException, GitAPIException {
        createSourceFile(projectDirectoryName, fileName, gitProjectDirectory);
        return commitProject(git, projectDirectoryName, "Yet another file");
    }

    private static void createSourceFile(String projectDirectoryName, String fileName, Path gitProjectDirectory) throws IOException {
        createSourceDirectoryIfNeeded(projectDirectoryName, gitProjectDirectory);
        Files.createFile(projectSourceDirectory(projectDirectoryName, gitProjectDirectory).resolve(fileName));
    }

    private static void createSourceDirectoryIfNeeded(String projectDirectoryName, Path gitProjectDirectory) throws IOException {
        if (Files.notExists(projectSourceDirectory(projectDirectoryName, gitProjectDirectory))) {
            Files.createDirectories(projectSourceDirectory(projectDirectoryName, gitProjectDirectory));
        }
    }

    private static Path projectSourceDirectory(String projectDirectoryName, Path gitProjectDirectory) {
        return projectDirectory(projectDirectoryName, gitProjectDirectory).resolve("src")
                                                                          .resolve("main")
                                                                          .resolve("java");
    }

    public static String firstLine(ByteArrayOutputStream redirectedSystemOut) {
        String[] lines = redirectedSystemOut.toString().split(System.lineSeparator());
        return lines[0];
    }
}