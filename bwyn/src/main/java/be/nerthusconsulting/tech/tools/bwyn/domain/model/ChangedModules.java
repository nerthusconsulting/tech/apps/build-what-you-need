package be.nerthusconsulting.tech.tools.bwyn.domain.model;

import com.google.common.base.Preconditions;

import java.util.List;

public record ChangedModules(References references, List<Module> modules) {
    public ChangedModules {
        Preconditions.checkNotNull(references, "references mustn't be null");
        Preconditions.checkNotNull(modules, "modules mustn't be null");
    }
}
