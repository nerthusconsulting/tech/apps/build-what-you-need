package be.nerthusconsulting.tech.tools.bwyn.domain.model;

import com.google.common.base.Preconditions;

public record Module(String path) {
    public Module {
        Preconditions.checkNotNull(path, "path mustn't be null");
    }
}
