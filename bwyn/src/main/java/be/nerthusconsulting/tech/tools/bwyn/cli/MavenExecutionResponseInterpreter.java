package be.nerthusconsulting.tech.tools.bwyn.cli;

import be.nerthusconsulting.tech.tools.bwyn.domain.model.MavenExecution;
import be.nerthusconsulting.tech.tools.bwyn.domain.model.References;
import be.nerthusconsulting.tech.tools.bwyn.domain.model.Response;
import com.google.common.base.Preconditions;

public class MavenExecutionResponseInterpreter {
    private static final String NO_CHANGED_MODULE_ERROR_FORMAT
            = "Aucun module n'a été modifié entre les références %s et %s";
    private final ErrorInterpreter errorInterpreter;

    public MavenExecutionResponseInterpreter(ErrorInterpreter errorInterpreter) {
        this.errorInterpreter = Preconditions.checkNotNull(errorInterpreter, "errorInterpreter mustn't be null");
    }

    public String interpret(Response<MavenExecution> mavenExecutionResponse) {
        if (mavenExecutionResponse.isError()) {
            return this.errorInterpreter.interpret(mavenExecutionResponse.error());
        } else {
            return this.mavenCommandRan(mavenExecutionResponse)
                   ? this.mavenCommandRanResult(mavenExecutionResponse.value())
                   : this.noChangedModuleError(mavenExecutionResponse.value().changedModules().references());
        }
    }

    private boolean mavenCommandRan(Response<MavenExecution> mavenExecutionResponse) {
        return mavenExecutionResponse.value().process() != null;
    }

    private String mavenCommandRanResult(MavenExecution mavenExecution) {
        return "Exécution de '"
               + String.join(" ", mavenExecution.fullCommands())
               + "'";
    }

    private String noChangedModuleError(References references) {
        return NO_CHANGED_MODULE_ERROR_FORMAT.formatted(references.firstReferenceName(),
                                                        references.secondReferenceName());
    }
}
