package be.nerthusconsulting.tech.tools.bwyn.domain.usecases;

import be.nerthusconsulting.tech.tools.bwyn.domain.model.Module;
import be.nerthusconsulting.tech.tools.bwyn.domain.model.*;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class RunMavenCommandOnChangedModulesUseCase {
    private final ListChangedModulesUseCase listChangedModulesUseCase;

    public RunMavenCommandOnChangedModulesUseCase(ListChangedModulesUseCase listChangedModulesUseCase) {
        this.listChangedModulesUseCase = listChangedModulesUseCase;
    }

    public Response<MavenExecution> execute(List<String> mavenCommands, References references) {
        return listChangedModulesUseCase.execute(references)
                                        .flatMap(changedModules -> createResponse(mavenCommands, changedModules));
    }

    private Response<MavenExecution> createResponse(List<String> mavenCommands, ChangedModules changedModules) {
        if (changedModules.modules().isEmpty()) {
            return noExecutionResponse(mavenCommands, changedModules);
        } else {
            return doExecute(mavenCommands, changedModules);
        }
    }

    private static Response<MavenExecution> noExecutionResponse(List<String> mavenCommands, ChangedModules changedModules) {
        return Response.result(new MavenExecution(mavenCommands, changedModules));
    }

    private Response<MavenExecution> doExecute(List<String> mavenCommands, ChangedModules changedModules) {
        Process mavenProcess = runMavenProcess(changedModules, mavenCommands);
        return Response.result(mavenExecution(mavenCommands, changedModules, mavenProcess));
    }

    private Process runMavenProcess(ChangedModules changedModules, List<String> mavenCommands) {
        try {
            Process mavenProcess = startMavenProcess(changedModules, mavenCommands);
            return mavenProcess.onExit().get();
        } catch (IOException | InterruptedException | ExecutionException e) {
            throw new IllegalStateException(e);
        }
    }

    private Process startMavenProcess(ChangedModules changedModules, List<String> mavenCommands) throws IOException {
        return buildMavenProcess(changedModules, mavenCommands).start();
    }

    private ProcessBuilder buildMavenProcess(ChangedModules changedModules, List<String> mavenCommands) {
        return new ProcessBuilder(fullMavenCommands(changedModules, mavenCommands))
                .directory(Path.of(System.getProperty("user.dir")).toFile())
                .inheritIO();
    }

    private List<String> fullMavenCommands(ChangedModules changedModules, List<String> mavenCommands) {
        List<String> fullCommands = new ArrayList<>();
        // TODO Rechercher une installation ou un wrapper Maven
        fullCommands.add("mvn");
        fullCommands.addAll(mavenCommands);
        fullCommands.add("--pl");
        fullCommands.addAll(changedModules.modules().stream().map(Module::path).toList());
        return fullCommands;
    }

    private MavenExecution mavenExecution(List<String> mavenCommands,
                                          ChangedModules changedModules,
                                          Process mavenProcess) {
        return new MavenExecution(mavenCommands, changedModules, mavenProcess);
    }
}
