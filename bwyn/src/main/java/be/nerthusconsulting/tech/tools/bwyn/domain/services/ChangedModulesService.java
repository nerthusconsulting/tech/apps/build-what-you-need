package be.nerthusconsulting.tech.tools.bwyn.domain.services;

import be.nerthusconsulting.tech.tools.bwyn.domain.model.Module;
import org.eclipse.jgit.diff.DiffEntry;

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ChangedModulesService {
    private static final Pattern SRC_PATTERN = Pattern.compile("([A-Za-z\\d/_-]+)/src/main/.+");
    private static final Pattern POM_PATTERN = Pattern.compile("([A-Za-z\\d/_-]+)/pom.xml");

    public List<Module> list(List<DiffEntry> differences) {
        return differences.stream()
                          .flatMap(this::differencePaths)
                          .map(this::extractModule)
                          .filter(Optional::isPresent)
                          .map(Optional::get)
                          .distinct()
                          .collect(Collectors.toList());
    }

    private Stream<String> differencePaths(DiffEntry difference) {
        return Stream.of(difference.getOldPath(), difference.getNewPath());
    }

    private Optional<Module> extractModule(String path) {
        return moduleMatchers(path).stream()
                                   .filter(Matcher::matches)
                                   .findFirst()
                                   .map(matcher -> matcher.group(1))
                                   .map(Module::new);
    }

    private List<Matcher> moduleMatchers(String path) {
        return List.of(srcMatcher(path), pomMatcher(path));
    }

    private Matcher srcMatcher(String path) {
        return SRC_PATTERN.matcher(path);
    }

    private Matcher pomMatcher(String path) {
        return POM_PATTERN.matcher(path);
    }
}