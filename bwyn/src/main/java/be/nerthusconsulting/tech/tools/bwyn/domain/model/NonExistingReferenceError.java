package be.nerthusconsulting.tech.tools.bwyn.domain.model;

import com.google.common.base.Preconditions;

import java.util.List;

public record NonExistingReferenceError(List<String> referencesNames) implements Error {
    public NonExistingReferenceError {
        Preconditions.checkNotNull(referencesNames, "referencesNames mustn't be null");
        Preconditions.checkArgument(referencesNames.size() >= 1 && referencesNames.size() <= 2, "referencesNames must have between 1 and 2 elements");
    }
}
