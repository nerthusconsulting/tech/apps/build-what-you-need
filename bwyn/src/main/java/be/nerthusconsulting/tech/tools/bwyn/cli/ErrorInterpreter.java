package be.nerthusconsulting.tech.tools.bwyn.cli;

import be.nerthusconsulting.tech.tools.bwyn.domain.model.Error;
import be.nerthusconsulting.tech.tools.bwyn.domain.model.NonExistingReferenceError;

import java.util.List;

public class ErrorInterpreter {
    private static final String NON_EXISTING_REFERENCE_ERROR_FORMAT
            = "La référence %s n'existe pas. Veuillez vérifier que vous ne vous êtes pas trompé";
    private static final String NON_EXISTING_REFERENCES_ERROR_FORMAT
            = "Les références %s et %s n'existent pas. Veuillez vérifier que vous ne vous êtes pas trompé";

    public String interpret(Error error) {
        if (error instanceof NonExistingReferenceError nonExistingReferenceError) {
            return this.nonExistingReferenceError(nonExistingReferenceError.referencesNames());
        } else {
            throw new IllegalStateException("Erreur non prise en charge : " + error.getClass());
        }
    }

    private String nonExistingReferenceError(List<String> referencesNames) {
        if (referencesNames.size() == 1) {
            return String.format(NON_EXISTING_REFERENCE_ERROR_FORMAT, referencesNames.get(0));
        } else {
            return String.format(NON_EXISTING_REFERENCES_ERROR_FORMAT, referencesNames.get(0), referencesNames.get(1));
        }
    }
}