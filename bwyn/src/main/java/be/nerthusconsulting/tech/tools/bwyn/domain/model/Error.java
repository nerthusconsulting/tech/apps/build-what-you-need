package be.nerthusconsulting.tech.tools.bwyn.domain.model;

public sealed interface Error permits NonExistingReferenceError {}
