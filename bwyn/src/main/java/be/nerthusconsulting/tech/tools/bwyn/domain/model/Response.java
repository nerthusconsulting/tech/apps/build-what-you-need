package be.nerthusconsulting.tech.tools.bwyn.domain.model;

import com.google.common.base.Preconditions;

import java.util.Objects;
import java.util.function.Function;

public final class Response<R> {
    public static <R> Response<R> result(R value) {
        Preconditions.checkNotNull(value, "value mustn't be null");
        return new Response<>(value, null);
    }

    public static <R> Response<R> error(Error error) {
        Preconditions.checkNotNull(error, "error mustn't be null");
        return new Response<>(null, error);
    }

    private final R result;
    private final Error error;

    private Response(R result, Error error) {
        this.result = result;
        this.error = error;
    }

    public R value() {
        return this.result;
    }

    public Error error() {
        return this.error;
    }

    public boolean isError() {
        return this.error != null;
    }

    public <T> Response<T> flatMap(Function<R, Response<T>> mapper) {
        if (isError()) {
            return new Response<>(null, this.error);
        } else {
            return mapper.apply(this.result);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Response<?> response = (Response<?>) o;
        return Objects.equals(this.result, response.result) && Objects.equals(this.error, response.error);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.result, this.error);
    }
}
