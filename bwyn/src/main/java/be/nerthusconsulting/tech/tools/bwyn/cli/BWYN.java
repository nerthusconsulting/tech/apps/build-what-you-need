package be.nerthusconsulting.tech.tools.bwyn.cli;

import be.nerthusconsulting.tech.tools.bwyn.domain.model.ChangedModules;
import be.nerthusconsulting.tech.tools.bwyn.domain.model.MavenExecution;
import be.nerthusconsulting.tech.tools.bwyn.domain.model.References;
import be.nerthusconsulting.tech.tools.bwyn.domain.model.Response;
import be.nerthusconsulting.tech.tools.bwyn.domain.services.ChangedModulesService;
import be.nerthusconsulting.tech.tools.bwyn.domain.services.GitService;
import be.nerthusconsulting.tech.tools.bwyn.domain.usecases.ListChangedModulesUseCase;
import be.nerthusconsulting.tech.tools.bwyn.domain.usecases.RunMavenCommandOnChangedModulesUseCase;
import picocli.CommandLine;

import java.util.List;

import static picocli.CommandLine.*;

@Command
public final class BWYN implements Runnable {
    @Parameters(index = "0", arity = "1")
    private String firstReferenceName;

    @Parameters(index = "1", arity = "0..1")
    private String secondReferenceName;

    @Option(names = "--mvn", arity = "1..*")
    private List<String> mavenCommands;

    public static void main(String... args) {
        new CommandLine(new BWYN()).setParameterExceptionHandler(new BwynParameterExceptionHandler())
                                   .execute(args);
    }

    @Override
    public void run() {
        System.out.println(result());
    }

    private String result() {
        if (mavenCommands == null) {
            return new ChangedModulesResponseInterpreter(new ErrorInterpreter()).interpret(changedModulesResponse());
        } else {
            return new MavenExecutionResponseInterpreter(new ErrorInterpreter()).interpret(mavenExecutionResponse());
        }
    }

    private Response<ChangedModules> changedModulesResponse() {
        return listChangedModulesUseCase().execute(references());
    }

    private ListChangedModulesUseCase listChangedModulesUseCase() {
        return new ListChangedModulesUseCase(new GitService(), new ChangedModulesService());
    }

    private References references() {
        return new References(this.firstReferenceName, this.secondReferenceName);
    }

    private Response<MavenExecution> mavenExecutionResponse() {
        return new RunMavenCommandOnChangedModulesUseCase(listChangedModulesUseCase())
                .execute(mavenCommands, references());
    }
}
