package be.nerthusconsulting.tech.tools.bwyn.domain.services;

import be.nerthusconsulting.tech.tools.bwyn.domain.model.References;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.List;

public class GitService {
    private static final Logger LOGGER = LoggerFactory.getLogger(GitService.class);

    public boolean referenceExists(String referenceName) {
        try (Repository repository = openRepository()) {
            return repository.resolve(referenceName) != null;
        } catch (IOException e) {
            LOGGER.error("Une erreur est survenue lors de la recherche de la référence");
            throw new IllegalStateException(e);
        }
    }

    private Repository openRepository() {
        try {
            return new FileRepositoryBuilder().setGitDir(gitDirectory().toFile())
                                              .build();
        } catch (IOException e) {
            LOGGER.error("Une erreur est survenue lors de l'ouverture du repository Git");
            throw new IllegalStateException(e);
        }
    }

    private Path gitDirectory() {
        return Path.of(workingDirectory(), ".git");
    }

    private String workingDirectory() {
        return System.getProperty("user.dir");
    }

    public List<DiffEntry> listDifferences(References references) {
        try (Repository repository = openRepository()) {
            CanonicalTreeParser firstReferenceTreeParser = canonicalTreeParser(repository, references.firstReferenceName());
            CanonicalTreeParser secondReferenceTreeParser = canonicalTreeParser(repository, references.secondReferenceName());

            DiffFormatter diffFormatter = new DiffFormatter(OutputStream.nullOutputStream());
            diffFormatter.setRepository(repository);
            return diffFormatter.scan(firstReferenceTreeParser, secondReferenceTreeParser);
        } catch (IOException e) {
            LOGGER.error("Une erreur est survenue lors de la récupération des différences");
            throw new IllegalStateException(e);
        }
    }

    private CanonicalTreeParser canonicalTreeParser(Repository repository, String referenceName) {
        try (ObjectReader reader = repository.newObjectReader()) {
            RevWalk walk = new RevWalk(repository);
            RevTree referenceTree = walk.parseTree(repository.resolve(referenceName));
            CanonicalTreeParser canonicalTreeParser = new CanonicalTreeParser();
            canonicalTreeParser.reset(reader, referenceTree);
            return canonicalTreeParser;
        } catch (IOException e) {
            LOGGER.error("Une erreur est survenue lors de la récupération de la référence {}", referenceName);
            throw new IllegalStateException(e);
        }
    }
}