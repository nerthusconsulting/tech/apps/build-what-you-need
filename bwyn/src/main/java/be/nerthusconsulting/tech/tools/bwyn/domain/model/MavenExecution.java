package be.nerthusconsulting.tech.tools.bwyn.domain.model;

import java.util.ArrayList;
import java.util.List;

public record MavenExecution(List<String> commands, ChangedModules changedModules, Process process) {
    public MavenExecution(List<String> commands, ChangedModules changedModules) {
        this(commands, changedModules, null);
    }
    public List<String> fullCommands() {
        List<String> fullCommands = new ArrayList<>();
        fullCommands.add("mvn");
        fullCommands.addAll(commands);
        fullCommands.add("--pl");
        fullCommands.addAll(changedModules.modules().stream().map(Module::path).toList());
        return fullCommands;
    }
}
