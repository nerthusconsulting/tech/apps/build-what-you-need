package be.nerthusconsulting.tech.tools.bwyn.cli;

import be.nerthusconsulting.tech.tools.bwyn.domain.model.Module;
import be.nerthusconsulting.tech.tools.bwyn.domain.model.*;
import com.google.common.base.Preconditions;

import java.util.List;
import java.util.stream.Collectors;

public class ChangedModulesResponseInterpreter {
    private static final String MODULE_FOUND_INFORMATION_FORMAT
            = "Le module suivant a été modifié entre les références %s et %s : %s";
    private static final String MODULES_FOUND_INFORMATION_FORMAT
            = "Les modules suivants ont été modifiés entre les références %s et %s : %s";
    private static final String NO_CHANGED_MODULE_ERROR_FORMAT
            = "Aucun module n'a été modifié entre les références %s et %s";
    private final ErrorInterpreter errorInterpreter;

    public ChangedModulesResponseInterpreter(ErrorInterpreter errorInterpreter) {
        this.errorInterpreter = Preconditions.checkNotNull(errorInterpreter, "errorInterpreter mustn't be null");
    }

    public String interpret(Response<ChangedModules> changedModulesResponse) {
        if (changedModulesResponse.isError()) {
            return this.errorInterpreter.interpret(changedModulesResponse.error());
        } else {
            return this.foundModules(changedModulesResponse)
                   ? this.modulesFoundInformation(changedModulesResponse.value())
                   : this.noChangedModuleError(changedModulesResponse.value().references());
        }
    }

    private boolean foundModules(Response<ChangedModules> changedModulesResponse) {
        return !changedModulesResponse.value().modules().isEmpty();
    }

    private String modulesFoundInformation(ChangedModules changedModules) {
        return this.modulesFoundInformationFormat(changedModules).formatted(
                changedModules.references().firstReferenceName(),
                changedModules.references().secondReferenceName(),
                this.commaSeparatedModules(changedModules.modules())
        );
    }

    private String modulesFoundInformationFormat(ChangedModules changedModules) {
        return this.hasOneModule(changedModules) ? MODULE_FOUND_INFORMATION_FORMAT : MODULES_FOUND_INFORMATION_FORMAT;
    }

    private boolean hasOneModule(ChangedModules changedModules) {
        return changedModules.modules().size() == 1;
    }

    private String commaSeparatedModules(List<Module> modules) {
        return modules.stream()
                      .map(Module::path)
                      .collect(Collectors.joining(","));
    }

    private String noChangedModuleError(References references) {
        return NO_CHANGED_MODULE_ERROR_FORMAT.formatted(references.firstReferenceName(),
                                                        references.secondReferenceName());
    }
}