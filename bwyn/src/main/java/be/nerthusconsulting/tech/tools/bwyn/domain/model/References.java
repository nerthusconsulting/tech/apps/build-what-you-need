package be.nerthusconsulting.tech.tools.bwyn.domain.model;

import com.google.common.base.Preconditions;

import java.util.List;

import static org.eclipse.jgit.lib.Constants.HEAD;

public record References(String firstReferenceName, String secondReferenceName) {
    public References(String firstReferenceName, String secondReferenceName) {
        Preconditions.checkNotNull(firstReferenceName, "firstReferenceName mustn't be null");
        this.firstReferenceName = firstReferenceName;
        this.secondReferenceName = computeSecondReferenceName(secondReferenceName);
    }

    private boolean twoReferenceWereGiven(String secondReferenceName) {
        return secondReferenceName != null;
    }

    public String computeSecondReferenceName(String secondReferenceName) {
        return this.twoReferenceWereGiven(secondReferenceName) ? secondReferenceName : HEAD;
    }

    public List<String> referencesNames() {
        return List.of(this.firstReferenceName, this.secondReferenceName);
    }
}
