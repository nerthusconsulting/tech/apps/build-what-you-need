package be.nerthusconsulting.tech.tools.bwyn.cli;

import picocli.CommandLine;

public class BwynParameterExceptionHandler implements CommandLine.IParameterExceptionHandler {
    private static final int MAXIMUM_AUTHORIZED_REFERENCES_COUNT = 2;

    @Override
    public int handleParseException(CommandLine.ParameterException parameterException, String[] args) {
        if (parameterException instanceof CommandLine.MissingParameterException missingParameterException) {
            if (missingParameterException.getMissing().stream().anyMatch(m -> m.paramLabel().equals("<firstReferenceName>"))) {
                parameterException.getCommandLine().getOut().println(missingReferenceError());
            } else if (missingParameterException.getMissing().stream().anyMatch(m -> m.paramLabel().equals("<mavenCommands>"))) {
                parameterException.getCommandLine().getOut().println(missingMavenCommandsError());
            }
        } else if (parameterException instanceof CommandLine.UnmatchedArgumentException unmatchedArgumentException) {
            parameterException.getCommandLine().getOut().println(tooManyReferencesError(unmatchedArgumentException));
        }
        return parameterException.getCommandLine().getExitCodeExceptionMapper().getExitCode(parameterException);
    }

    private String missingReferenceError() {
        return "BWYN accepte entre une et deux références, aucune n'a été fournie";
    }

    private String missingMavenCommandsError() {
        return "BWYN --mvn accepte au minimum un argument";
    }

    private String tooManyReferencesError(CommandLine.UnmatchedArgumentException unmatchedArgumentException) {
        return "BWYN accepte entre une et deux références, " + referencesCount(unmatchedArgumentException) + " ont été fournies";
    }

    private int referencesCount(CommandLine.UnmatchedArgumentException unmatchedArgumentException) {
        return MAXIMUM_AUTHORIZED_REFERENCES_COUNT + unmatchedArgumentException.getUnmatched().size();
    }
}
