package be.nerthusconsulting.tech.tools.bwyn.domain.usecases;

import be.nerthusconsulting.tech.tools.bwyn.domain.model.Module;
import be.nerthusconsulting.tech.tools.bwyn.domain.model.*;
import be.nerthusconsulting.tech.tools.bwyn.domain.services.ChangedModulesService;
import be.nerthusconsulting.tech.tools.bwyn.domain.services.GitService;
import org.eclipse.jgit.diff.DiffEntry;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.function.Predicate.not;

public class ListChangedModulesUseCase {
    private final GitService gitService;
    private final ChangedModulesService changedModulesService;

    public ListChangedModulesUseCase(GitService gitService, ChangedModulesService changedModulesService) {
        this.gitService = gitService;
        this.changedModulesService = changedModulesService;
    }

    public Response<ChangedModules> execute(References references) {
        List<String> nonExistingReferencesNames = nonExistingReferencesNames(references);
        if (!nonExistingReferencesNames.isEmpty()) {
            return Response.error(new NonExistingReferenceError(nonExistingReferencesNames));
        }

        return Response.result(changedModules(references));
    }

    private List<String> nonExistingReferencesNames(References references) {
        return references.referencesNames()
                         .stream()
                         .filter(not(this.gitService::referenceExists))
                         .collect(Collectors.toList());
    }

    private ChangedModules changedModules(References references) {
        return new ChangedModules(references, changedModulesList(references));
    }

    private List<Module> changedModulesList(References references) {
        return changedModulesService.list(differences(references));
    }

    private List<DiffEntry> differences(References references) {
        return gitService.listDifferences(references);
    }
}
