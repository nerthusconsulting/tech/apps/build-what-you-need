# Build What You Need (BWYN)
Build What You Need (BWYN) is a CLI tool aimed at extracting a list of Maven modules modified between to Git commits.

The idea is then to use that list to build only what you need of your Maven project:
```bash
mvn clean install --pl project-a,project
```

This tool is best suited for large monoliths which takes a lot of time to build completely and for which we'd rather avoid to use a plain:
```bash
mvn clean install
```

## Download
The latest version of bwyn can be found through the links below:
- [Linux](https://gitlab.com/api/v4/projects/34864904/jobs/artifacts/main/download?job=artifacts-linux)
- [MacOS](https://gitlab.com/api/v4/projects/34864904/jobs/artifacts/main/download?job=artifacts-macos)
- [Windows](https://gitlab.com/api/v4/projects/34864904/jobs/artifacts/main/download?job=artifacts-windows)