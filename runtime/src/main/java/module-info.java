module be.nerthusconsulting.tech.tools.bwyn.runtime {
    requires java.base;
    requires java.logging;
    requires java.management;
    requires java.naming;
    requires java.security.jgss;
    requires java.sql;
    requires java.xml;
}